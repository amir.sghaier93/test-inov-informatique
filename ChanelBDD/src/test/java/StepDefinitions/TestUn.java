package StepDefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import managers.FileReaderManager;
import pages.LandingPage;


public class TestUn extends BaseClass {
	private final String expectedUrl="https://www.chanel.com/us/";
	LandingPage landingPage;
public String secElement;
	@Given("^User is in Landing Page$")
	public void user_is_in_landing_page() {
		landingPage();
		landingPage= new LandingPage(driver);

	}
	@And("^checks the website has displayed well")
	public void checks_the_website_has_displayed_well () {
		String checkSiteOK= driver.getCurrentUrl();
		assertEquals("Le lien du site n'est pas OK",expectedUrl, checkSiteOK);
	}
	@And("^chanel logo is displayed")
	public void chanel_logo_is_displayed() {
		checkLogoDisplayed();
	}
	@When ("he clicks on the search and search for {string}")
	public void search_for_rouge_allure(String rougeAllure) {
		landingPage.searchIcon().click();
		String getPlaceHolder= landingPage.placeHolder().getAttribute("type");
		assertEquals(getPlaceHolder, "search");
		Actions action= new Actions(driver);
		action.moveToElement(landingPage.searchInput()).click().sendKeys(rougeAllure).perform();
		landingPage.searchBtn().click();
	}
	@Then("the elements in the displayed List contains the word {string}")
	public void check_Elements_contains_rougeAllure(String word) {
		List<WebElement> lst=driver.findElements(By.xpath("//h4//span"));
		List<String> strings = new ArrayList<String>();
		for(WebElement e : lst){
			strings.add(e.getText());
		}
		for(String b : strings){
			boolean containRougeAllure=  b.contains(word);
			assertTrue(containRougeAllure,"Erreur l'element ne contient pas ALLURE ROUGE mais contient: "+b );
		}
		System.out.println(strings);
		String getSecondElementText=strings.get(1).toUpperCase();
		System.out.println(getSecondElementText);
		secElement=getSecondElementText;
	}
		@When("^he select the second element in list")
		public void select_second_element() {
		WebElement secondElement= driver.findElement(By.xpath("(//a[@class='product-list-inline__link'])[2]"));
		secondElement.click();
		
	}
		@Then("^the product name is the same in the element list")
		public void compare_product_name_and_product_in_list() {
		String getNomProduit= landingPage.nomProduitAfficher().getText();
		assertEquals("Le nom du produit numero deux n'est le meme que celui dans la liste des elements",getNomProduit, secElement);

	}


}
