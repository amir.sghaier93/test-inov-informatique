package StepDefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import managers.FileReaderManager;
import pages.LandingPage;


public class TestDeux extends BaseClass {
	private final String expectedUrl="https://www.chanel.com/us/";
	LandingPage landingPage;
public String secElement;

	@When("^he Clicks on the makeup menu and choose lipstick products$")
	public void goToLipstickProducts() {
		landingPage();
		landingPage= new LandingPage(driver);
		landingPage.makeUpMenu().click();
		landingPage.lipstick().click();
	}
	@And("^he add the Rouge Allure product to bag$")
	public void addRougeAllureToBag() throws InterruptedException {
		rougeAllureAddToBag();
		Thread.sleep(3000);
		landingPage.addToBagInSidePanel().click();
	}
	@And("navigate to shopping bag page")
	public void openShoppingBagPage() {
		landingPage.afficherPanier().click();
		assertEquals(driver.getCurrentUrl(), expectedUrlPanier);
	}
	@Then("the product Allure rouge is in the bag")
	public void the_product_allure_rouge_is_in_the_bag() {
		String verifProduit= landingPage.nomProduitPanier().getText();
		assertEquals(verifProduit, "rouge allure");
	}
	@And("the quantity is equal to one")
	public void the_quantity_is_equal_to_one() {
		checkSelectedQuantityIsOne(); 
	}
	@Then("the total price is equal to the product price")
	public void the_total_price_is_equal_to_the_product_price() {
		String prixProduit= driver.findElement(By.xpath("//td[@class='is-bold js-cart-price-subtotal']")).getText();
		String totalPrixProduit= driver.findElement(By.xpath("/html/body/main/div[3]/div/div/div/div[2]/div/table/tbody/tr[4]/td")).getText();
		boolean prixProduitEndWith=Pattern.compile("45.00$").matcher(prixProduit).find();
		boolean totalPrixEndWith=Pattern.compile("45.00$").matcher(totalPrixProduit).find();
		if (!totalPrixEndWith || !prixProduitEndWith) {
	fail("Le total prix n'est pas �gale au prix produit");
		}
	}

	}