package StepDefinitions;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import dataProvider.ConfigFileReader;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
String test= "";
	int i=0;
	public	String	expectedUrlPanier= "https://www.chanel.com/us/cart";
	public WebDriver driver;
	ConfigFileReader configFileReader;
	public void landingPage() {
		configFileReader= new ConfigFileReader();
		System.setProperty("webdriver.chrome.driver", configFileReader.getDriverPath());
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);
		driver.get(configFileReader.getApplicationUrl());
	}
	public void checkLogoDisplayed() {
		int logoChanel= driver.findElements(By.xpath("//img[@class='logo_header']")).size();
		if (logoChanel!=1) {
			fail("Erreur dans l'affichage de la page d'acceuil");	
	}	
}
	public void rougeAllureAddToBag() {
		List<WebElement> lst=driver.findElements(By.xpath("//a[@data-test='product_link']//span[2]"));
		List<String> strings = new ArrayList<String>();
		for(WebElement e : lst){
			strings.add(e.getText());
			
		}
		for (String element : strings){
			i++;
	         if (element.equals("rouge allure")){ 

break;
	         }
}
		WebElement addToBag= driver.findElement(By.xpath("(//button[@class='button is-tertiary'])["+i+"]"));
		   JavascriptExecutor executor = (JavascriptExecutor)driver;
		    executor.executeScript("arguments[0].click();", addToBag);

	}
	public void checkSelectedQuantityIsOne() {
		try {
			String checkSelected=	driver.findElement(By.xpath("//option[1]")).getAttribute("selected");

		} catch (Exception e) {
	String getSelectedQuantity= driver.findElement(By.xpath("//option[@selected='selected']")).getText();

		fail("L'utilisateur a choisit la quantite de 1 Lipstick mais dans le panier la quantité est égale a :" +getSelectedQuantity);
		}
	}
	}


