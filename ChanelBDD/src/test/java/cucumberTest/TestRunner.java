package cucumberTest;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		monochrome = true,
// dryRun = true,
				plugin = {"pretty",	
				"html:target/reports/report.html",
				"json:target/cucumber-report.json"			
		},
		
		features = "src/test/java/features", glue="StepDefinitions")
public class TestRunner {

}
