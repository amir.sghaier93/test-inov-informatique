package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {
	public WebDriver driver;
public LandingPage(WebDriver driver) {
	this.driver= driver;
}
By searchIcon= By.xpath("//button[@data-test='btnSearch_Header_Disabled']");
public WebElement searchIcon() {
	return driver.findElement(searchIcon);
}
By placeHolder= By.id("searchInput");
public WebElement placeHolder() {
	return driver.findElement(placeHolder);
}
By searchBtn= By.xpath("//button[@class='button search__button search__submit js-search-submit']");
public WebElement searchBtn() {
	return driver.findElement(searchBtn);
}
By searchInput= By.id("searchInput");
public WebElement searchInput() {
	return driver.findElement(searchInput);
}
By nomProduitAfficher= By.xpath("//span[@class='heading product-details__title ']");
public WebElement nomProduitAfficher() {
	return driver.findElement(nomProduitAfficher);
}
By makeUpMenu= By.id("makeup");
public WebElement makeUpMenu() {
	return driver.findElement(makeUpMenu);
}
By lipstick= By.xpath("//a[@data-linkname='Lipstick']");
public WebElement lipstick() {
	return driver.findElement(lipstick);
}
By addToBagInSidePanel= By.xpath("//button[@class='button is-primary is-fullwidth']");
public WebElement addToBagInSidePanel() {
	return driver.findElement(addToBagInSidePanel);
}
By afficherPanier= By.xpath("//a[@class='button is-primary is-block js-reviewBag-cta']");
public WebElement afficherPanier() {
	return driver.findElement(afficherPanier); 
}
By nomProduitPanier= By.xpath("//div[@class='cart-product__description']//span[@class='heading is-7']");
public WebElement nomProduitPanier() {
	return driver.findElement(nomProduitPanier); 
}

}