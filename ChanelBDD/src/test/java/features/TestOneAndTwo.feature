Feature: TestOne
  Background: User navigate to Chanel landing page
    Given User is in Landing Page
  Scenario: Test one
  
    And checks the website has displayed well
    And chanel logo is displayed
    When he clicks on the search and search for "ROUGE ALLURE"
    Then the elements in the displayed List contains the word "rouge allure"
    When he select the second element in list
    Then the product name is the same in the element list
    
    Scenario: Test Two
    When he Clicks on the makeup menu and choose lipstick products
    And he add the Rouge Allure product to bag
    And navigate to shopping bag page
    Then the product Allure rouge is in the bag
    And the quantity is equal to one
    And the total price is equal to the product price