package chanelTests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.List;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LandingPage;
import global.Global;

public class TestOne extends Global{
	private final String expectedUrl="https://www.chanel.com/us/";
	LandingPage landingPage;
	@BeforeTest
	public void LandingPage() {
		landingPage();
		landingPage= new LandingPage(driver);
	}
	@Test(priority = 0)
	public void rougeAllure() {
		//  1- Acc�s au site --> OK si le site s'affiche
		String checkSiteOK= driver.getCurrentUrl();
		assertEquals(expectedUrl, checkSiteOK,"Le lien du site n'est pas OK");
		// Check Logo Chanel is displayed and check that the website is loaded properly
		checkLogoDisplayed();
		// 2- Cliquer sur la barre de recherche (dans le menu du haut) 
		landingPage.searchIcon().click();
		//Check the user clicked on the search Icon
		String getPlaceHolder= landingPage.placeHolder().getAttribute("type");
		assertEquals(getPlaceHolder, "search");
		//    3- Chercher le mot "ROUGE ALLURE"
		Actions action= new Actions(driver);
		action.moveToElement(landingPage.searchInput()).click().sendKeys("ROUGE ALLURE").perform();
		landingPage.searchBtn().click();
		//4- V�rifier si les �l�ments de la liste affich�e contiennent le mot recherch�
		List<WebElement> lst=driver.findElements(By.xpath("//h4//span"));
		List<String> strings = new ArrayList<String>();
		for(WebElement e : lst){
			strings.add(e.getText());
		}
		for(String b : strings){
			boolean containRougeAllure=  b.contains("rouge allure");
			assertTrue(containRougeAllure,"Erreur l'element ne contient pas ALLURE ROUGE mais contient: "+b );
		}
		String getSecondElementText=strings.get(1).toUpperCase();
		// 5- S�lectionner le deuxi�me �l�ment de la liste 
		WebElement secondElement= driver.findElement(By.xpath("(//a[@class='product-list-inline__link'])[2]"));
		secondElement.click();
		// 6 S'assurer que le nom du produit affich� et le m�me que celui s�lectionn� auparavant.
		String getNomProduit= landingPage.nomProduitAfficher().getText();
		assertEquals(getNomProduit, getSecondElementText,"Le nom du produit numero deux n'est le meme que celui dans la liste des elements");

	}
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}

