package chanelTests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;
import java.util.regex.Pattern;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LandingPage;
import global.Global;
import junit.framework.AssertionFailedError;

public class TestTwo extends Global{
	private final String expectedUrl="https://www.chanel.com/us/";
	LandingPage landingPage;
	@BeforeTest
	public void LandingPage() {
		landingPage();
		landingPage= new LandingPage(driver);
	}
	@Test(priority = 0)
	public void rougeAllure() throws InterruptedException {
		//  1- Acc�s au site
		String checkSiteOK= driver.getCurrentUrl();
		assertEquals(expectedUrl, checkSiteOK,"Le lien du site n'est pas OK");
//		 2- Acc�s � la cat�gorie Makeup => Lipstick (dans le menu du haut) 
landingPage.makeUpMenu().click();
landingPage.lipstick().click();
// Ajouter le produit au panier
rougeAllureAddToBag();
Thread.sleep(1500);
landingPage.addToBagInSidePanel().click();

	}
	@Test(priority = 1)
	public void PanierChanel() {
// Check L'utilisateur est redirige vers l'url panier
		landingPage.afficherPanier().click();
	assertEquals(driver.getCurrentUrl(), expectedUrlPanier);
//  OK si produit "ROUGE ALLURE" est dans le panier,
	String verifProduit= landingPage.nomProduitPanier().getText();
	assertEquals(verifProduit, "rouge allure","le nom prdouit est suppos� etre Rouge Allure mais le produit affich� est: " +verifProduit);
	// la quantit�=1 
	checkSelectedQuantityIsOne();
	// le total panier est �gal au prix du produit
	String prixProduit= driver.findElement(By.xpath("//td[@class='is-bold js-cart-price-subtotal']")).getText();
	String totalPrixProduit= driver.findElement(By.xpath("/html/body/main/div[3]/div/div/div/div[2]/div/table/tbody/tr[4]/td")).getText();
	boolean prixProduitEndWith=Pattern.compile("45.00$").matcher(prixProduit).find();
	boolean totalPrixEndWith=Pattern.compile("45.00$").matcher(totalPrixProduit).find();
	if (!totalPrixEndWith || !prixProduitEndWith) {
fail("Le total prix n'est pas �gale au prix produit");
	}
	}
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}

